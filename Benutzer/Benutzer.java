package com.company;

public class Benutzer {


    private String passwort;
    private String name;

    public Benutzer(String name, String password) {
        this.passwort = password;
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getName() {
        return this.name;
    }

    public String getPasswort() {
        return this.passwort;
    }

}